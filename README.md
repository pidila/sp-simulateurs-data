# Simulateurs de la DILA sur service-public.fr


Le site officiel de l'administration française service-public.fr référence une soixantaine de simulateurs disponibles pour répondre à un large éventail de questions administratives qui se posent aux particuliers et aux professionnels.

Parmi ces similateurs, la DILA en a développé quinze :

* [Simulateur du coût du certificat d'immatriculation](https://www.service-public.fr/simulateur/calcul/cout-certificat-immatriculation)
* [Simulateur de calcul des intérêts moratoires des marchés publics](https://www.service-public.fr/simulateur/calcul/interets-moratoires)
* [Simulateur de calcul de la gratification minimale d'un stagiaire](https://www.service-public.fr/simulateur/calcul/gratification-stagiaire)
* [Simulateur de calcul de pension alimentaire](https://www.service-public.fr/simulateur/calcul/pension-alimentaire)
* [Simulateur de calcul des droits de succession](https://www.service-public.fr/simulateur/calcul/droits-succession)
* [Simulateur : Faut-il un certificat médical pour obtenir une licence sportive ?](https://www.service-public.fr/simulateur/calcul/certificatMedical)
* [Simulateur : Calendrier des vacances scolaires de votre département](https://www.service-public.fr/simulateur/calcul/Dates_Vacances_Scolaires)
* [Calculateur de prix HT ou TTC](https://www.service-public.fr/simulateur/calcul/convertisseurPrixHTouTTC)
* [Simulateur : connaître la zone de sa commune : 1, 1 bis, 2 ou 3](https://www.service-public.fr/simulateur/calcul/zones)
* [Simulateur : savoir si un logement est situé en zone tendue](https://www.service-public.fr/simulateur/calcul/zones-tendues)
* [Simulateur : barème fiscal de l'usufruit et de la nue-propriété](https://www.service-public.fr/simulateur/calcul/bareme-fiscal-usufruit/particuliers)
* [Simulateur des indemnités en cas de licenciement abusif](https://www.service-public.fr/simulateur/calcul/bareme-indemnites-prudhomales)
* [Simulateur des frais de mise en location imputables au locataire](https://www.service-public.fr/simulateur/calcul/frais-locataire)
* [Simulateur : connaître la zone de sa commune : Abis, A, B1, B2 ou C](https://www.service-public.fr/simulateur/calcul/zonage-abc)
* [Simulateur : calculer un prix après réduction](https://www.service-public.fr/simulateur/calcul/CalculReduction)


## Simulateur du coût du certificat d'immatriculation
Ce simulateur du coût de carte grise (certificat d’immatriculation) est disponible sur le site service-public.fr depuis le 5 avril 2016.

Le montant de la carte grise se compose de différentes taxes, comme la taxe régionale ou le malus écologique, dont l’application et le calcul peuvent se révéler difficile.

Le simulateur de calcul permet donc de réaliser une estimation du coût de la carte grise d’un véhicule (voiture, 2 roues, camionnette, camion, etc.) à l’occasion de l’achat d’un véhicule neuf ou d’occasion mais aussi pour plus d’une dizaine d’autres démarches telles que, par exemple :
* la mise à jour de l’adresse en cas de déménagement ;
* une demande de duplicata de la carte grise lorsque celle-ci a été perdue, volée ou détériorée ;
* la modification du titulaire du certificat à la suite d’un mariage ou d’un divorce ;
* l’établissement d’un nouveau certificat lorsque toutes les cases réservées au contrôle technique ont été utilisées.

**Fichiers utilisés :** 
* Règles de calcul : cout-certificat-immatriculation.xml
* Source de données : cout-certificat-immatriculation.json
* Schéma de la source de données : cout-certificat-immatriculation.schema.json

## Simulateur de calcul des intérêts moratoires des marchés publics
Ce simulateur d’intérêts moratoires  permet de calculer les pénalités de retard de paiement dans le cadre d’un contrat régit par la réglementation des marchés publics.
Lors de l'exécution d'un marché public, si l'administration ne respecte pas les délais réglementaires pour payer son fournisseur, et son sous-traitant le cas échéant, des pénalités financières sont appliquées.

**Fichiers utilisés :** 
* Règles de calcul : interets-moratoires.xml
* Source de données : interets-moratoires.json
* Schéma de la source de données : interets-moratoires.schema.json

## Simulateur de calcul de la gratification minimale d'un stagiaire
En avril 2015, à la demande du ministère de l’Education nationale, de l’Enseignement supérieur et de la Recherche, la DILA a conçu et mis en ligne sur service-public.fr ce simulateur permettant aux employeurs et aux étudiants de calculer le montant de la gratification minimale rendue obligatoire pour tout stage supérieur à 2 mois, par la loi  n°2014-788 du 10 juillet 2014 tendant au développement, à l'encadrement des stages et à l'amélioration du statut des stagiaires.
À partir de la date de signature de la convention de stage et du nombre d’heures de présence effective du stagiaire dans l’organisme d’accueil, ce simulateur permet de calculer :
* le montant de la gratification minimale due pour chaque mois du stage (gratification mensuelle),
* le montant total de la gratification due pour toute la durée du stage (gratification totale),
* le montant mensuel à verser en cas de lissage de la gratification sur la totalité de la durée du stage (gratification mensuelle lissée).

**Fichiers utilisés :** 
* Règles de calcul : gratification-stagiaire.xml
* Source de données : gratification-stagiaire.json
* Schéma de la source de données : gratification-stagiaire.schema.json

## Simulateur de calcul de pension alimentaire
Permet de calculer le montant de la pension alimentaire en fonction du nombre d'enfant et des revenus du parent débiteur.
Attention : il s'agit d'une simple estimation. Seul le juge, s'il est saisi, peut prononcer le montant définitif de la pension en prenant en compte la situation spécifique des parents

**Fichiers utilisés :** 
* Règles de calcul : pension-alimentaire.xml
* Source de données : pension-alimentaire.json
* Schéma de la source de données : pension-alimentaire.schema.json

## Simulateur de calcul des droits de succession
Ce simulateur propose de réaliser une estimation indicative des droits de succession redevables suite au décès d'un proche. Pour réaliser cette simulation, l'utilisateur doit connaître la valeur des biens qui composent la succession et le montant de la part lui revenant, ainsi que le montant des dettes laissées par le défunt.

**Fichiers utilisés :** 
* Règles de calcul : droits-succession.xml
* Source de données : droits-succession.json
* Schéma de la source de données : droits-succession.schema.json

## Simulateur : Faut-il un certificat médical pour obtenir une licence sportive ?
Ce simulateur vous indique s'il faut fournir un certificat médical pour obtenir ou renouveler une licence sportive (loisir ou compétition) auprès de votre fédération (football, tennis, équitation, judo...). Selon les cas, il vous informe sur la durée de validité de votre certificat médical. Il vous permet de répondre au questionnaire santé et d'attester de votre état de santé qui, le cas échéant, vous dispense de fournir un certificat médical.

Pour plus d'informations, consulter [Un certificat médical est-il obligatoire pour faire du sport ?](https://www.service-public.fr/particuliers/vosdroits/F1030) et [À quoi sert une licence sportive ?](https://www.service-public.fr/particuliers/vosdroits/F1029)

**Fichiers utilisés :** 
* Règles de calcul : certificatMedical.xml
* Source de données : federation.json
* Schéma de la source de données : federation.schema.json

## Simulateur : Calendrier des vacances scolaires de votre département
Les dates des vacances scolaires dépendent de la zone dans laquelle se trouve l'établissement (école maternelle ou primaire, collège ou lycée). Ce simulateur vous permet de consulter le calendrier scolaire 2018-2019 et 2019-2020 de votre département.

**Fichiers utilisés :** 
* Règles de calcul : Dates_Vacances_Scolaires.xml
* Source de données : VacancesScolaires.json
* Schéma de la source de données : VacancesScolaires.schema.json

## Calculateur prix HT ou TTC
Ce convertisseur permet de calculer un prix hors taxes (HT) à partir d'un prix toutes taxes comprises (TTC) et vice versa selon les différents taux de TVA applicables. Les taux de la taxe sur la valeur ajoutée (TVA) dépendent de la nature du produit ou du service, du mode de consommation (produits alimentaires) et du lieu d'achat du produit ou de la prestation de service (France métropolitaine, Corse ou Guadeloupe/Martinique/Réunion).

**Fichiers utilisés :** 
* Règles de calcul : convertisseurPrixHTouTTC.xml

## Simulateur : connaître la zone de sa commune : 1, 1 bis, 2 ou 3
Ce simulateur vous permet de connaître la zone géographique (1, 1 bis, 2 ou 3) dont dépend le logement concerné. Cette zone détermine pour partie le plafond de ressources permettant de percevoir une allocation logement (APL, ALF, ALS) ou une réduction du loyer de solidarité (RLS) pour les locataires d'un logement social (HLM). Le plafond de ressources est également fixé en fonction de la composition de votre foyer.

**Fichiers utilisés :** 
* Règles de calcul : zones.xml
* Source de données : UnDeuxTroisZonage.json
* Schéma de la source de données : UnDeuxTroisZonage.schema.json

## Simulateur : savoir si un logement est situé en zone tendue
Ce simulateur vous permet de déterminer si le logement est situé en zone tendue. La zone détermine :
* Pour un terrain nu : le droit à l’exonération de la taxe foncière sur les propriétés non bâties ;
* Pour un logement vacant : l’application de la taxe sur les logements vacants applicable à certaines communes (TLV) ;
* Pour un logement loué :
  * le droit à un préavis d’1 mois pour le locataire (logement vide ou loi 1948) ;
  * l’application de l’encadrement des loyers (logement vide ou meublé).

**Fichiers utilisés :** 
* Règles de calcul : zones-tendues.xml
* Source de données : zonage-commune.json
* Schéma de la source de données : zonage-commune.schema.json

## Simulateur : barème fiscal de l'usufruit et de la nue-propriété
Ce simulateur permet de connaître la répartition de la valeur d'un bien entre l'usufruitier et le nu-propriétaire en cas de démembrement du droit de propriété.

**Fichiers utilisés :** 
* Règles de calcul : bareme-fiscal-usufruit.xml
* Source de données : PourcentUsufruit.json
* Schéma de la source de données : PourcentUsufruit.schema.json

## Simulateur des indemnités en cas de licenciement abusif
Ce simulateur indique les montants minimum et maximum des indemnités pour dommages et intérêts susceptibles d'être fixées par le juge prud'homal en cas de licenciement irrégulier ou sans cause réelle et sérieuse.

Ces planchers et plafonds, qui s'imposent désormais au juge, ne concernent que des licenciements notifiés au salarié après le 23 septembre 2017.

**Fichiers utilisés :** 
* Règles de calcul : bareme-indemnites-prudhomales.xml
* Source de données : bareme-indemnites-prudhomales.json
* Schéma de la source de données : bareme-indemnites-prudhomales.schema.json

## Simulateur des frais de mise en location imputables au locataire
Ce simulateur vous permet de connaître la zone géographique dont dépend le logement concerné.

Le résultat obtenu (zone très tendue, zone tendue, reste du territoire) détermine le prix maximum TTC par m2 de surface habitable qui peut être demandé au locataire pour rémunérer les services de l’agent immobilier lors de la mise en location (hors état des lieux) : visite du logement, constitution du dossier du locataire et rédaction du contrat de bail

**Fichiers utilisés :** 
* Règles de calcul : frais-locataire.xml
* Source de données : zonage-commune.json
* Schéma de la source de données : zonage-commune.schema.json

## Simulateur : connaître la zone de sa commune : Abis, A, B1, B2 ou C
Ce simulateur vous permet de connaître le type de zone (A, Abis, B1, B2 ou C) dont dépend le logement concerné.
La zone détermine :
* le revenu maximum pour avoir droit à un logement social ;
* le revenu maximum pour avoir droit au prêt à taux zéro ou prêt d’accession sociale ;
* pour un bailleur, le droit à une réduction d’impôt ;
* pour le bailleur d'un logement conventionné avec l'Anah :
* le droit à une déduction fiscale sur les revenus fonciers ;
* le revenu maximum du candidat locataire ;
* le loyer initial maximum.

**Fichiers utilisés :** 
* Règles de calcul : zonage-abc.xml
* Source de données : zonage-commune.json
* Schéma de la source de données : zonage-commune.schema.json

## Simulateur : calculer un prix après réduction
Ce simulateur permet de calculer le prix après l'application d'un taux de réduction (soldes, promotion, remise, rabais, ristourne...).

**Fichiers utilisés :** 
* Règles de calcul : CalculReduction.xml


# Simulateurs de la DILA sur boamp.fr

La DILA a développé 3 simulateurs BOAMP à l¹attention des acheteurs publics et des entreprises titulaires d'un marché public :

* Pour les acheteurs publics, le BOAMP propose 2 simulateurs afin de les guider dans l’utilisation de ses services : un simulateur d’aide au choix du formulaire et un simulateur choix du forfait.
    * [Simulateur d’aide au choix des formulaires de publication](https://simulateurs.boamp.fr/simulateur/calcul/formulaires)
    * [Simulateur d'aide au choix du forfait BOAMP](https://simulateurs.boamp.fr/simulateur/calcul/forfaits)
* Pour les entreprises, le BOAMP met à disposition un simulateur de calcul des intérêts moratoires applicables dans le cadre de l¹exécution de marchés publics.
    * [Simulateur de calcul des intérêts moratoires des marchés publics](https://simulateurs.boamp.fr/simulateur/calcul/interets-moratoires/boamp)

## Simulateur d’aide au choix des formulaires de publication
Ce simulateur permet de déterminer le formulaire à utiliser pour publier son avis parmi la quarantaine de formulaires proposés par le BOAMP. En répondant à 4 questions, l’acheteur public est orienté vers le formulaire correspondant à ses attentes. Il prend en compte les modifications apportées par les directives européennes et les nouveaux décrets relatifs aux marchés publics, aux marchés défense ou sécurité et aux concessions entrés en vigueur le 1er avril 2016.

**Fichiers utilisés :** 
* Règles de calcul : formulaires.xml

## Simulateur d'aide au choix du forfait BOAMP
Ce simulateur permet aux acheteurs publics de déterminer le forfait le mieux adapté à leurs besoins de publication en vue de bénéficier de tarifs préférentiels.
Il leur permet d’optimiser leur budget de publication en faisant une estimation des unités de publication nécessaires aux différents types d’avis à publier dans l’année (sur la base de l'année précédente par exemple).

**Fichiers utilisés :** 
* Règles de calcul : forfaits.xml

## Simulateur de calcul des intérêts moratoires des marchés publics
Ce simulateur d’intérêts moratoires permet de calculer les pénalités de retard de paiement dans le cadre d’un contrat régit par la réglementation des marchés publics. Lors de l'exécution d'un marché public, si l'administration ne respecte pas les délais réglementaires pour payer son fournisseur, et son sous-traitant le cas échéant, des pénalités financières sont appliquées.

**Fichiers utilisés :** 
* Règles de calcul : interets-moratoires.xml
* Source de données : interets-moratoires.json
* Schéma de la source de données : interets-moratoires.schema.json

# Mise à disposition des sources
Les données et les règles qui permettent de réaliser ces simulations sont mises à la disposition du public sous [Licence ouverte](https://www.etalab.gouv.fr/licence-ouverte-open-licence)  dans le cadre du PGO (Partenariat Gouvernement ouvert).

Les simulateurs utilisent le moteur de simulation G6K (https://github.com/eureka2/G6K) qui est une application PHP développée avec le framework Symfony 2.

Les sources de données de référence (fournies) pour la simulation peuvent être stockées dans une base de données MySQL, PostgreSQL ou SQLite. 

## Procédure d'installation
### Pré-requis pour Symfony 2
* PHP Version 5.3.3 + (recommandé 5.5.9+)
* JSON activé
* ctype
* date.timezone dans php.ini
* module PHP-XML
* version 2.6.21+ de libxml
* PHP tokenizer
* les modules mbstring, iconv, POSIX (seulement sur *nix), Intl avec ICU 4+, et APCU 3.0.17+ APC (fortement recommandé)
* paramètres php.ini recommandés :
  * short_open_tag = Off
  * magic_quotes_gpc = Off
  * register_globals = Off
  * session.auto_start = Off
* mod_rewrite doit être activé


### Pré-requis pour G6K
  * PDO activé
  * pdo_pgsql ou pdo_mysql ou pdo_sqlite activé selon le cas
  * pgsql and ou sqlite3 activé selon le cas
  * SimpleXML activé
  * serialize_precision = -1

### Installation du moteur de simulation
1. Créer une base de données à l'aide de l'outil d'administration de votre SGBDR.
2. Se placer dans le &lt;DOCUMENT_ROOT&gt; de votre serveur Web
3. Télécharger composer.phar (https://getcomposer.org/download/1.2.1/composer.phar) et le placer dans &lt;DOCUMENT_ROOT&gt;
4. Sous un shell *nix ou sous DOS, exécuter la commande : ``php -d memory_limit=-1 composer.phar create-project eureka2/g6k simulateur/`` 
5. Entrer les valeurs de paramètres demandées par l'installateur, notamment :
  1. database_driver => pdo_pgsl, pdo_mysql ou pdo_sqlite
  2. database_host => le nom ou l'adresse IP de votre serveur de base de données (faire simplement &lt;Enter&gt; dans le cas de SQLite)
  3. database_port => port du serveur de base de données  (faire simplement &lt;Enter&gt; dans le cas de SQLite)
  4. database_name => nom de la base de données créée en 1.  (faire simplement &lt;Enter&gt; dans le cas de SQLite)
  5. database_user => nom de l'utilisateur permettant de se connecter à la base de données (faire simplement &lt;Enter&gt; dans le cas de SQLite). Cet utilisateur doit disposer du privilège "CREATE DATABASE" pendant la phase d'installation.
  6. database_password => mot de passe de cet utilisateur (faire simplement &lt;Enter&gt; dans le cas de SQLite)
  7. database_path => utilisé dans le cas de SQLite et ignoré dans les autres cas, donc faire &lt;Enter&gt;
  8. locale => fr

Vous pouvez ignorer tous les autres paramètres en faisant &lt;Enter&gt;.

6. Ajouter une directive "AllowOverride All" pour le répertoire "simulateur" dans la configuration de votre serveur Apache
7. Donner le droit d'écriture à l'utilisateur Apache sur les répertoires "simulateur/app/cache", "simulateur/app/logs" et "simulateur/src/EUREKA/G6KBundle/Resources"

À ce stade, ont été installés :
* le moteur de simulation dans &lt;DOCUMENT_ROOT&gt;/simulateur ainsi que tous les composants Symfony 2 nécessaires à son fonctionnement;
* deux utilisateurs dans la base de données : un utilisateur admin (mot de passe "admin1") et un utilisateur "invité" (Guest);
* un simulateur de démo pour le calcul de pension alimentaire (en anglais pour l'instant). Ce simulateur permet de vérifier le fonctionnement du moteur de simulation après installation.

### Vérification de l'installation
1. Vérifier à l'aide de l'outil d'administration de votre SGBDR que les deux tables "fos_user" et "MARate" ont bien été créées dans la base de données.
2. Si, par exemple, votre serveur Web est installé dans localhost, ouvrir le lien http://localhost/simulateur/calcul/demo dans vote navigateur Internet préféré, l'écran suivant doit apparaitre :

![Copie d'écran de la démo pension alimentaire](screenshots/Maintenance-allowance-demo.png)

### Installation des simulateurs de la DILA
1. Se connecter à l'interface d'administration avec le compte admin/admin1. L'interface d'administration est à l'adresse http://localhost/simulateur/calcul/admin. **A noter que cet interface est en cours de développement et n'est donc pas utilisable en production.**
2. Cliquer sur l'item "Sources de données" de la barre de menu, l'écran ci-dessous d'affiche:
![Copie d'écran de l'interface admin import datasource](screenshots/import-datasources.png)
3. Cliquer sur le bouton "Importer source de données", l'écran ci-dessous d'affiche:
![Copie d'écran de l'interface admin import données cout-certificat-immatriculation](screenshots/import-datasource-cout-immatriculation.png)
4. Uploader les fichiers "cout-certificat-immatriculation.schema.json" et "cout-certificat-immatriculation.json" que l'on trouve dans le répertoire "donnees-de-reference" de ce dépôt git.
5. Après un import réussi, l'écran ci-dessous s'affiche montrant toutes les tables créées dans la base de données.
![Copie d'écran de l'interface admin données cout-certificat-immatriculation après import](screenshots/import-datasource-cout-immatriculation-result.png)
6. Cliquer sur l'item "Simulateurs" de la barre de menu, l'écran ci-dessous s'affiche :
![Copie d'écran de l'interface admin import simulateur](screenshots/import-simulateurs.png)
7. Cliquer sur le bouton "Importer un simulateur", l'écran ci-dessous d'affiche:
![Copie d'écran de l'interface admin import du simulateur cout-certificat-immatriculation](screenshots/import-simulateur-cout-immatriculation.png)
8. Uploader les fichiers "cout-certificat-immatriculation.xml" et "cout-certificat-immatriculation.css" que l'on trouve respectivement dans les répertoires "definitions-des-simulateurs" et "feuilles-de-styles" de ce dépôt git.
9. Après un import réussi, l'écran ci-dessous s'affiche :
![Copie d'écran de l'interface admin simulateur cout-certificat-immatriculation après import](screenshots/import-simulateur-cout-immatriculation-result.png)
10. Cliquer sur le bouton "Tester" en haut à gauche pour faire fonctionner le simulateur.
![Copie d'écran du simulateur cout-certificat-immatriculation](screenshots/simulateur-cout-certificat-immatriculation.png)


**Répéter les étapes 2 à 10 pour installer les autres simulateurs.**

## À propos du moteur de simulation
Ce moteur est disponible sous licence MIT, et propose les fonctionnalités suivantes : 
* La simulation en ligne. La DILA l'utilise pour dix-neuf simulateurs en ligne sur ses sites.
* La gestion des utilisateurs de l'interface d'administration (super admin, admin, contributeur, ...)
* La gestion (création, mise à jour, import) des sources de données de référence internes (bases de données) et externes (via des web services)
* L'import et l'export de simulateurs
* La création de simulateurs depuis l'interface d'administration
* La modification de simulateurs depuis l'interface d'administration

## Copyright et licence

2016 DILA - Direction de l'information légale et administrative. Données sous licence ETALAB. 